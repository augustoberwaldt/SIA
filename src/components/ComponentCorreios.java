package components;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;

import javax.rmi.CORBA.Util;

import org.json.JSONObject;

public class ComponentCorreios extends Component {

	
	
	public JSONObject  buscaCep(String cep) throws IOException{
		  
            this.start();  
	        String requestURL = "http://cep.correiocontrol.com.br/"+cep+".json"; 
	        URL obj = new URL(requestURL);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");

			int responseCode = con.getResponseCode();
			
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
		    in.close();
           System.out.println(response.toString()); 
	       return  new JSONObject(response.toString());
	}	
	
	
	
}

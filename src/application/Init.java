package application;

import javax.persistence.Persistence;

import Utils.LoadManager;
import Utils.Loader;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Init {
	
	/*
     * 
     * Inicializa Processo Banco
    */
    {
    	LoadManager.getEntityManager();
	}

    /*
     * 
     * Inicializa Processo Aplicacao
    */
	public	Init() 
	{
		Stage stage	= new Stage();
		stage.initStyle(StageStyle.UNDECORATED);
		Loader.load(getClass().getResource("/views/Login.fxml"), stage, true ,"Login");
	}
	/*
	 * 
	 * Finaliza Processo Banco
	 */
	public void closeConnection() {
		LoadManager.close();
	}
	

}

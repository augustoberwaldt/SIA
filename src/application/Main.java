package application;
	
import javax.swing.JOptionPane;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	
	@Override
	public void start(Stage primaryStage) {
		 new Init(); 
	}
	
	public static void main(String[] args) {
		launch(args);
	}
		
	@Override
	public void stop()
	{
	     new Init().closeConnection(); 	 
	}
	
}

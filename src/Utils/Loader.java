package Utils;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
/**
 * 
 * 
 * @author Augusto Berwaldt
 *
 */
public class Loader   extends FXMLLoader
{
    
	private static Scene scene ;
	private static Stage stage;
	private static Parent root ;
	private static FXMLLoader loder;
	private static Map<String,Parent> fila = new HashMap<String,Parent>() ; 
	
 
    public static  void load(URL url,Stage stage,boolean show,String tagName )
    {
    	try{
    		
    		Loader.stage=stage;
	    	FXMLLoader loder= new  FXMLLoader();
	        String tmp_url ="/views/"+url; 
		    loder.setLocation(url);
		    Loader.root= loder.load(); 
		    fila.put(tagName,Loader.root);
		    
		    if(show)
		    {
		    	Loader.scene = new Scene(Loader.root) ; 
				Loader.stage.setScene(scene);
		    	Loader.stage.show();	
		    }
		    
    	}catch(IOException ie)
    	{
    		ie.printStackTrace(); 
    		System.out.println(ie);
    	}
    }
  


	public static Stage getStage() {
		return stage;
	}
	public static void setStage(Stage stage) {
		Loader.stage = stage;
	}
	public void  show()
    {
    	
	    Loader.stage.show();
    	
    }
    public static void  setUrl(URL url)
    {
    	Loader.loder= new  FXMLLoader();
    	loder.setLocation(url);
    }
  
    public static Parent  getComponet()
    {
    	return  Loader.root;
    }
    
    public static void  setScene(Scene scene)
    {
    	 Loader.scene=scene;
    }
    
    public static Scene  getScene()
    {
    	return  Loader.scene;
    }

	public static Map<String, Parent> getFila() {
		// TODO Auto-generated method stub
		return fila;
	}

	
	 
   
}

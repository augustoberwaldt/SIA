package Utils;

import java.math.BigInteger;
import java.security.MessageDigest;

public class Encrypt {

	public static String MD5(String string) {

		MessageDigest m;

		try {
			m = MessageDigest.getInstance("MD5");
			m.update(string.getBytes(), 0, string.length());
			BigInteger i = new BigInteger(1, m.digest());

			string = String.format("%1$032X", i);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return string;
	}

}
package models;

import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.dom4j.rule.Mode;

import Utils.LoadManager;
import Utils.Loader;

public class UsuarioGrupoDao extends App_Model implements IPersisteDao<UsuarioGrupo> {


	@Override
	public boolean save(UsuarioGrupo o) {
		
		EntityManager manager = LoadManager.getEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		
		manager.persist(o);
		tx.commit();
		manager.close();
		//LoadManager.close();

		return true;
	}

	@Override
	public int update(UsuarioGrupo o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int remove(UsuarioGrupo o) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public List<UsuarioGrupo> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UsuarioGrupo find(UsuarioGrupo o) {
		
		EntityManager manager = LoadManager.getEntityManager();
		UsuarioGrupo  result =	manager.find(UsuarioGrupo.class,o.getId());
		return result;
	}
	
	public ObservableList<UsuarioGrupo> findAllObservableList()
	{
		ObservableList<UsuarioGrupo> data = FXCollections.observableArrayList();
		
		EntityManager manager = LoadManager.getEntityManager();
		Query query = manager.createQuery("from UsuarioGrupos");
		List<UsuarioGrupo> usuariosG = query.getResultList();
		for (UsuarioGrupo usuarioGrupo : usuariosG) 
		{
			data.add(usuarioGrupo);
		}
	    return data;	
	}


	
}

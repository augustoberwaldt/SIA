package models;

import java.util.Collection;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ManyToAny;

@Entity(name="cursos")
public class Curso {
	
	
	private int _id;
	
	private IntegerProperty id;
	
	private IntegerProperty codigo;

	private StringProperty nome;
	
	private StringProperty descricao;

	private IntegerProperty numeroSemestres;

	private IntegerProperty status;
	
	private Collection<Disciplina> disciplina ;
	
	private StringProperty turno;
	
	private StringProperty nivel;
	
	
	
	public Curso(){
		 codigo= new SimpleIntegerProperty();
		 nome = new SimpleStringProperty();
		 descricao = new SimpleStringProperty();
		 numeroSemestres= new SimpleIntegerProperty();
		 status = new SimpleIntegerProperty();
		 turno = new SimpleStringProperty();
		 nivel = new SimpleStringProperty();
	}
	
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		if (id == null) {
			return _id;
		} else {
			return id.get();
		}
	}

	@ManyToMany()
	@JoinTable( name = "curso_disciplina",joinColumns = {@JoinColumn(name = "curso_id")},inverseJoinColumns={@JoinColumn(name= "discipina_id")} )
	public Collection<Disciplina> getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(Collection<Disciplina> disciplina) {
		this.disciplina = disciplina;
	}

	public void setId(int id) {
		if (this.id == null) {
			_id = id;
		} else {
			this.id.set(id);
		}
	}

	public IntegerProperty idProperty() {
		if (id == null) {
			id = new SimpleIntegerProperty(this, "id", _id);
		}
		return id;
	}


	public final IntegerProperty codigoProperty() {
		return this.codigo;
	}


	@Column
	public final int getCodigo() {
		return this.codigoProperty().get();
	}



	public final void setCodigo(final int codigo) {
		this.codigoProperty().set(codigo);
	}



	public final StringProperty nomeProperty() {
		return this.nome;
	}


	@Column
	public final java.lang.String getNome() {
		return this.nomeProperty().get();
	}



	public final void setNome(final java.lang.String nome) {
		this.nomeProperty().set(nome);
	}



	public final StringProperty descricaoProperty() {
		return this.descricao;
	}

	@Column
	public final java.lang.String getDescricao() {
		return this.descricaoProperty().get();
	}



	public final void setDescricao(final java.lang.String descricao) {
		this.descricaoProperty().set(descricao);
	}



	public final IntegerProperty numeroSemestresProperty() {
		return this.numeroSemestres;
	}


	@Column
	public final int getNumeroSemestres() {
		return this.numeroSemestresProperty().get();
	}



	public final void setNumeroSemestres(final int numeroSemestres) {
		this.numeroSemestresProperty().set(numeroSemestres);
	}



	public final IntegerProperty statusProperty() {
		return this.status;
	}


	@Column
	public final int getStatus() {
		return this.statusProperty().get();
	}



	public final void setStatus(final int status) {
		this.statusProperty().set(status);
	}

	public final StringProperty turnoProperty() {
		return this.turno;
	}
	
	@Column(name = "turno")
	public final java.lang.String getTurno() {
		return this.turnoProperty().get();
	}

	public final void setTurno(final java.lang.String turno) {
		this.turnoProperty().set(turno);
	}

	public final StringProperty nivelProperty() {
		return this.nivel;
	}
	
	@Column(name = "nivel")
	public final java.lang.String getNivel() {
		return this.nivelProperty().get();
	}

	public final void setNivel(final java.lang.String nivel) {
		this.nivelProperty().set(nivel);
	}


	@Override
	public String toString() {
		return "Curso [_id=" + _id + ", id=" + id + ", codigo=" + codigo
				+ ", nome=" + nome + ", descricao=" + descricao
				+ ", numeroSemestres=" + numeroSemestres + ", status=" + status
				+ ", disciplina=" + disciplina + ", turno=" + turno
				+ ", nivel=" + nivel + "]";
	}
		
}

package models;

import java.util.List;

import javafx.collections.ObservableList;

public interface IPersisteDao<T> {

	public  boolean save(T o);
	
	public  int update(T o);
	
	public  int remove(T o);
	
	public  T find(T o);
	
	public  List<T> findAll();
	
	public ObservableList<T> findAllObservableList();
	
	
}

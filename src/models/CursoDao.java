package models;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import Utils.LoadManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class CursoDao  extends App_Model implements IPersisteDao<Curso>{

	@Override
	public boolean save(Curso o) {
		
	    System.out.println("-----+"+o);
		EntityManager manager = LoadManager.getEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		manager.persist(o);
		tx.commit();
		manager.close();
		return true;
	}

	@Override
	public int update(Curso o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int remove(Curso o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Curso find(Curso o) {
		EntityManager manager = LoadManager.getEntityManager();
		Curso  result =	manager.find(Curso.class,o.getId());
		return result;
	}

	@Override
	public List<Curso> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<Curso> findAllObservableList() {
		 ObservableList<Curso> data = FXCollections.observableArrayList();
			
		 EntityManager manager = LoadManager.getEntityManager();
		 Query query = manager.createQuery("from cursos");
		 List<Curso> cursos = query.getResultList();
		 for (Curso Curso : cursos) 
		 {
				data.add(Curso);
		 }
		 return data;
	}
	
	public ObservableList<String> findAllObservableListNome() {
		ObservableList<String> data = FXCollections.observableArrayList();
			
			EntityManager manager = LoadManager.getEntityManager();

			Query query = manager.createQuery("from cursos");
		
			List<Curso> cursos = query.getResultList();
			for (Curso curso : cursos) 
			{
				data.add(curso.getNome());
			}
		    return data;
		}

}

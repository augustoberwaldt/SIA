package models;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import Utils.LoadManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PessoaDao extends App_Model implements IPersisteDao<Pessoa> {

	@Override
	public boolean save(Pessoa o) {
		EntityManager manager = LoadManager.getEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		manager.persist(o);
		tx.commit();
		manager.close();


		return true;
	}

	@Override
	public int update(Pessoa o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int remove(Pessoa o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Pessoa find(Pessoa o) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Pessoa> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<Pessoa> findAllObservableList() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public ObservableList<String> findAllObservableListNomeProfessor() {
		ObservableList<String> data = FXCollections.observableArrayList();
			
			EntityManager manager = LoadManager.getEntityManager();

			Query query = manager.createQuery("from pessoas  where  tipo = 2");
		
			List<Pessoa> pessoas = query.getResultList();
			for (Pessoa pessoa : pessoas) 
			{
				System.out.println(pessoa.getNome()); 
				data.add(pessoa.getNome());
			}
		    return data;
	}

}

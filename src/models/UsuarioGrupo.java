package models;

import java.sql.Date;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="UsuarioGrupos")
public class UsuarioGrupo {
	
	public static int  ADM = 1;
	public static int  ALUNO = 2;
	public static int  PROFESSOR = 3;
	
	private  IntegerProperty id;
	
	private  int _id;
	
	private  StringProperty  ident;
   	
    private StringProperty nome;
    
    private  IntegerProperty status ;
    
    private  Date data ;

    
    public UsuarioGrupo(){
    	this.id = new SimpleIntegerProperty();
    	this.nome = new SimpleStringProperty();
    	this.ident = new SimpleStringProperty();
    	this.status= new SimpleIntegerProperty();
    }
    @Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		if (id == null) {
			return _id;
		} else {
			return id.get();
		}
	}

	public void setId(int id) {
		if (this.id == null) {
			_id = id;
		} else {
			this.id.set(id);
		}
	}


	public IntegerProperty idProperty() {
		if (id == null) {
			id = new SimpleIntegerProperty(this, "id", _id);
		}
		return  id;
	}

	public final StringProperty nomeProperty() {
		return this.nome;
	}
	
	@Column(name = "nome", nullable = false)
	public final java.lang.String getNome() {
		return this.nomeProperty().get();
	}

	public final void setNome(final java.lang.String nome) {
		this.nomeProperty().set(nome);
	}

	public final IntegerProperty statusProperty() {
		return this.status;
	}
	@Column(name = "status", nullable = false)
	public final int getStatus() {
		return this.statusProperty().get();
	}

	public final void setStatus(final int status) {
		this.statusProperty().set(status);
	}
	public final StringProperty identProperty() {
		return this.ident;
	}
	@Column(name = "ident", nullable = false)
	public final java.lang.String getIdent() {
		return this.identProperty().get();
	}
	public final void setIdent(final java.lang.String ident) {
		this.identProperty().set(ident);
	}
    
 

    
    
}

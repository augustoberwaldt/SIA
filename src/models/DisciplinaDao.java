package models;

import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import Utils.LoadManager;

public class DisciplinaDao extends App_Model implements IPersisteDao<Disciplina>{

	@Override
	public boolean save(Disciplina o) {
		EntityManager manager = LoadManager.getEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		manager.persist(o);
		tx.commit();
		manager.close();
		return true;
	}

	@Override
	public int update(Disciplina o) {
		EntityManager manager = LoadManager.getEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		manager.merge(o);
		tx.commit();
		manager.close();
		return 1;
	}

	@Override
	public int remove(Disciplina o) {
		EntityManager manager = LoadManager.getEntityManager();
		manager.getTransaction().begin();
		manager.remove( manager.contains(o) ? o : manager.merge(o)  );
		manager.getTransaction().commit();
		manager.close();
		
		return 1;
	}

	@Override
	public Disciplina find(Disciplina o) {
		EntityManager manager = LoadManager.getEntityManager();
		Disciplina  result =	manager.find(Disciplina.class,o.getId());
		return result;
	}

	@Override
	public List<Disciplina> findAll() {
		EntityManager manager = LoadManager.getEntityManager();
		Query query = manager.createQuery("from disciplinas");
		List<Disciplina> usuarios = query.getResultList();
		return  usuarios;
	}

	@Override
	public ObservableList<Disciplina> findAllObservableList() {
        ObservableList<Disciplina> data = FXCollections.observableArrayList();
		
		EntityManager manager = LoadManager.getEntityManager();
		Query query = manager.createQuery("from disciplinas");
		List<Disciplina> disciplinas = query.getResultList();
		for (Disciplina disciplina : disciplinas) 
		{
			data.add(disciplina);
		}
	    return data;
	}
	
	public ObservableList<String> findAllObservableListNome() {
		ObservableList<String> data = FXCollections.observableArrayList();
			
			EntityManager manager = LoadManager.getEntityManager();

			Query query = manager.createQuery("from disciplinas");
		
			List<Disciplina> disciplinas = query.getResultList();
			for (Disciplina disciplina : disciplinas) 
			{
				data.add(disciplina.getNome());
			}
		    return data;
		}
	

}

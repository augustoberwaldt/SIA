package models;

import java.util.Collection;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ManyToAny;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


@Entity(name="matriculas")
public class Matricula {
    
	private IntegerProperty id ;
	
	private int _id ;
	
	private int situacao;

	private Date data; 
   
	private StringProperty codigo ;
	
    public  Matricula(){
    	this.id = new SimpleIntegerProperty() ;
    	    	
    	this.situacao = 0;
       
    	this.codigo = new SimpleStringProperty() ;
	  
    } 
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		if (id==null) {
			return _id ;
		} else {
			return id.get();
		}
	}
	@Column(name="data")
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	

	public void setId(int id) {
		if (this.id==null) {
			_id=id ;
		} else {
			this.id.set(id);
		}
	}
	
	public IntegerProperty idProperty() {
		if (id==null) {
			id = new SimpleIntegerProperty(this, "id", _id);
		}
		return id ;
	}
	@Column(name="situacao") 
	public int getSituacao() {
		return situacao;
	}
	public void setSituacao(int situacao) {
		this.situacao = situacao;
	}
	public final StringProperty codigoProperty() {
		return this.codigo;
	}
	@Column(name="codigo")
	public final java.lang.String getCodigo() {
		return this.codigoProperty().get();
	}
	public final void setCodigo(final java.lang.String codigo) {
		this.codigoProperty().set(codigo);
	}

	
}

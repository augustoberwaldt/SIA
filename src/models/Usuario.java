package models;

import java.sql.Date;
import java.util.Arrays;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cascade;

@Entity(name = "usuarios")
public class Usuario {

	private IntegerProperty id;

	private int _id;

	private StringProperty username;

	private StringProperty password;

	private UsuarioGrupo usuariogrupos;
	
	private Pessoa pessoa;

	private Matricula matricula;

	private Date data;

	
	private IntegerProperty status;

	private StringProperty cep;

	private StringProperty endereco;

	private StringProperty complemento;

	private StringProperty bairro;

	private StringProperty cidade;

	private StringProperty uf;

	private IntegerProperty numero;
	

	private byte[] foto;

	public Usuario() {

		this.id = new SimpleIntegerProperty();
		this._id = new Integer(0);
		this.username = new SimpleStringProperty();
		this.password = new SimpleStringProperty();
		this.usuariogrupos = new UsuarioGrupo();
		this.matricula = new Matricula();
		this.data = data;
		this.status = new SimpleIntegerProperty();
		this.cep = new SimpleStringProperty();
		this.endereco = new SimpleStringProperty();
		this.complemento = new SimpleStringProperty();
		this.bairro = new SimpleStringProperty();
		this.cidade = new SimpleStringProperty();
		this.uf = new SimpleStringProperty();
		this.numero = new SimpleIntegerProperty();
		this.foto = null;
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		if (id == null) {
			return _id;
		} else {
			return id.get();
		}
	}

	public void setId(int id) {
		if (this.id == null) {
			_id = id;
		} else {
			this.id.set(id);
		}
	}

	public IntegerProperty idProperty() {
		if (id == null) {
			id = new SimpleIntegerProperty(this, "id", _id);
		}
		return id;
	}

	@Lob
	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}
	@ManyToOne()
	@JoinColumn(name = "pessoa_id", nullable=true)
	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	@ManyToOne()
	@JoinColumn(name = "matricula_id", nullable=true)
	public Matricula getMatricula() {
		return matricula;
	}

	public void setMatricula(Matricula matricula) {
		this.matricula = matricula;
	}

	@ManyToOne
	@JoinColumn(name = "usuariogrupos_id")
	public UsuarioGrupo getUsuariogrupos() {
		return usuariogrupos;
	}

	public void setUsuariogrupos(UsuarioGrupo usuariogrupos) {
		this.usuariogrupos = usuariogrupos;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@Column(name = "data", nullable = true)
	public Date getData() {
		return this.data;
	}

	public final StringProperty usernameProperty() {
		return this.username;
	}

	@Column(name = "username", nullable = true)
	public final java.lang.String getUsername() {
		return this.usernameProperty().get();
	}

	public final void setUsername(final java.lang.String username) {
		this.usernameProperty().set(username);
	}

	public final StringProperty passwordProperty() {
		return this.password;
	}

	public final java.lang.String getPassword() {
		return this.passwordProperty().get();
	}

	public final void setPassword(final java.lang.String password) {
		this.passwordProperty().set(password);
	}

	public final IntegerProperty statusProperty() {
		return this.status;
	}

	public final java.lang.Integer getStatus() {
		return this.statusProperty().get();
	}

	public final void setStatus(final java.lang.Integer status) {
		this.statusProperty().set(status);
	}

	public final StringProperty cepProperty() {
		return this.cep;
	}

	public final java.lang.String getCep() {
		return this.cepProperty().get();
	}

	public final void setCep(final java.lang.String cep) {
		this.cepProperty().set(cep);
	}

	public final StringProperty enderecoProperty() {
		return this.endereco;
	}

	public final java.lang.String getEndereco() {
		return this.enderecoProperty().get();
	}

	public final void setEndereco(final java.lang.String endereco) {
		this.enderecoProperty().set(endereco);
	}

	public final StringProperty complementoProperty() {
		return this.complemento;
	}

	public final java.lang.String getComplemento() {
		return this.complementoProperty().get();
	}

	public final void setComplemento(final java.lang.String complemento) {
		this.complementoProperty().set(complemento);
	}

	public final StringProperty bairroProperty() {
		return this.bairro;
	}

	public final java.lang.String getBairro() {
		return this.bairroProperty().get();
	}

	public final void setBairro(final java.lang.String bairro) {
		this.bairroProperty().set(bairro);
	}

	public final StringProperty cidadeProperty() {
		return this.cidade;
	}

	public final java.lang.String getCidade() {
		return this.cidadeProperty().get();
	}

	public final void setCidade(final java.lang.String cidade) {
		this.cidadeProperty().set(cidade);
	}

	public final StringProperty ufProperty() {
		return this.uf;
	}

	public final java.lang.String getUf() {
		return this.ufProperty().get();
	}

	public final void setUf(final java.lang.String uf) {
		this.ufProperty().set(uf);
	}

	public final IntegerProperty numeroProperty() {
		return this.numero;
	}

	public final int getNumero() {
		return this.numeroProperty().get();
	}

	public final void setNumero(final int numero) {
		this.numeroProperty().set(numero);
	}

}

package models;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Table;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
@Entity(name="disciplinas")
public class Disciplina extends App_Model {

	private IntegerProperty id;
	
	private int _id;
	
	private StringProperty nome;

	private StringProperty ementa;

	private StringProperty objetivo;

	private StringProperty conteudo;

	private StringProperty referencia;
	

	

	public Disciplina(){
		
		this.nome = new  SimpleStringProperty();

		this.ementa = new  SimpleStringProperty() ;

		this.objetivo = new  SimpleStringProperty();

		this.conteudo = new  SimpleStringProperty() ;

		this.referencia = new  SimpleStringProperty() ;
		
	}
	@Id()
	@Column(name = "id" )
	@GeneratedValue(strategy = GenerationType.AUTO )
	public int getId() {
		if (id == null) {
			return _id;
		} else {
			return id.get();
		}
	}

	public void setId(int id) {
		if (this.id == null) {
			_id = id;
		} else {
			this.id.set(id);
		}
	}

	public IntegerProperty idProperty() {
		if (id == null) {
			id = new SimpleIntegerProperty(this, "id", _id);
		}
		return id;
	}
	public final StringProperty nomeProperty() {
		return this.nome;
	}

	public final java.lang.String getNome() {
		return this.nomeProperty().get();
	}

	public final void setNome(final java.lang.String nome) {
		this.nomeProperty().set(nome);
	}

	public final StringProperty ementaProperty() {
		return this.ementa;
	}

	public final java.lang.String getEmenta() {
		return this.ementaProperty().get();
	}

	public final void setEmenta(final java.lang.String ementa) {
		this.ementaProperty().set(ementa);
	}

	public final StringProperty objetivoProperty() {
		return this.objetivo;
	}

	public final java.lang.String getObjetivo() {
		return this.objetivoProperty().get();
	}

	public final void setObjetivo(final java.lang.String objetivo) {
		this.objetivoProperty().set(objetivo);
	}

	public final StringProperty conteudoProperty() {
		return this.conteudo;
	}

	public final java.lang.String getConteudo() {
		return this.conteudoProperty().get();
	}

	public final void setConteudo(final java.lang.String conteudo) {
		this.conteudoProperty().set(conteudo);
	}

	public final StringProperty referenciaProperty() {
		return this.referencia;
	}

	public final java.lang.String getReferencia() {
		return this.referenciaProperty().get();
	}

	public final void setReferencia(final java.lang.String referencia) {
		this.referenciaProperty().set(referencia);
	}

   @Override	 
   public String toString(){
	   return getNome();
   }
}

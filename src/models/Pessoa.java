package models;



import java.sql.Date;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity(name="pessoas")
public class Pessoa extends App_Model  {
	

	private  int  _id ;
    
	private IntegerProperty id;

	private IntegerProperty status;
	
	private StringProperty nome;
	
	private StringProperty email;
	
	private StringProperty areaAtuacao;
	
	private StringProperty setor;
	
	private FloatProperty salario;
	
	
	private Date  dataIncresso;
	
	private int tipo;
	
	public Pessoa(){
		this. id = new SimpleIntegerProperty();

		this.status =new SimpleIntegerProperty();
		
		this.nome = new SimpleStringProperty();
		
		this.email = new SimpleStringProperty();
		
		this.areaAtuacao = new SimpleStringProperty();
		
		this.setor =new SimpleStringProperty();
		
		this.salario =new SimpleFloatProperty();
		
		
	}
	
	@Column(name = "dataIncresso")
	public Date getDataIncresso() {
		return dataIncresso;
	}
	public void setDataIncresso(Date dataIncresso) {
		this.dataIncresso = dataIncresso;
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		if (id == null) {
			return _id;
		} else {
			return id.get();
		}
	}
	@Column(name = "tipo")
	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public void setId(int id) {
		if (this.id == null) {
			_id = id;
		} else {
			this.id.set(id);
		}
	}

	public IntegerProperty idProperty() {
		if (id == null) {
			id = new SimpleIntegerProperty(this, "id", _id);
		}
		return id;
	}

	public final IntegerProperty statusProperty() {
		return this.status;
	}
	@Column(name = "status")
	public final int getStatus() {
		return this.statusProperty().get();
	}

	public final void setStatus(final int status) {
		this.statusProperty().set(status);
	}

	public final StringProperty nomeProperty() {
		return this.nome;
	}
	@Column(name = "nome")
	public final java.lang.String getNome() {
		return this.nomeProperty().get();
	}

	public final void setNome(final java.lang.String nome) {
		this.nomeProperty().set(nome);
	}

	public final StringProperty emailProperty() {
		return this.email;
	}
	@Column(name = "email")
	public final java.lang.String getEmail() {
		return this.emailProperty().get();
	}

	public final void setEmail(final java.lang.String email) {
		this.emailProperty().set(email);
	}

	public final StringProperty areaAtuacaoProperty() {
		return this.areaAtuacao;
	}
	@Column(name = "areaAtuacao")
	public final java.lang.String getAreaAtuacao() {
		return this.areaAtuacaoProperty().get();
	}

	public final void setAreaAtuacao(final java.lang.String areaAtuacao) {
		this.areaAtuacaoProperty().set(areaAtuacao);
	}

	public final StringProperty setorProperty() {
		return this.setor;
	}
	@Column(name = "setor")
	public final java.lang.String getSetor() {
		return this.setorProperty().get();
	}

	public final void setSetor(final java.lang.String setor) {
		this.setorProperty().set(setor);
	}

	public final FloatProperty salarioProperty() {
		return this.salario;
	}
	@Column(name = "salario")
	public final float getSalario() {
		return this.salarioProperty().get();
	}

	public final void setSalario(final float salario) {
		this.salarioProperty().set(salario);
	}
 
}

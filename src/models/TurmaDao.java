package models;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import Utils.LoadManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class TurmaDao extends App_Model implements IPersisteDao<Turma> {

	@Override
	public boolean save(Turma o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int update(Turma o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int remove(Turma o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Turma find(Turma o) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Turma> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<Turma> findAllObservableList() {
	ObservableList<Turma> data = FXCollections.observableArrayList();
		
		EntityManager manager = LoadManager.getEntityManager();
		Query query = manager.createQuery("from turmas");
	
		List<Turma> turmas = query.getResultList();
		for (Turma turma : turmas) 
		{
			data.add(turma);
		}
	    return data;
	}
	

	public ObservableList<String> findAllObservableListNome() {
	ObservableList<String> data = FXCollections.observableArrayList();
		
		EntityManager manager = LoadManager.getEntityManager();

		Query query = manager.createQuery("from turmas");
	
		List<Turma> turmas = query.getResultList();
		for (Turma turma : turmas) 
		{
			data.add(turma.getNome());
		}
	    return data;
	}
	

}

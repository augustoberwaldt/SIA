package models;

import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.dom4j.rule.Mode;

import Utils.LoadManager;
import Utils.Loader;

public class UsuariosDao extends App_Model implements IPersisteDao<Usuario> {


	@Override
	public boolean save(Usuario o) {
		
		EntityManager manager = LoadManager.getEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		System.out.println(o.getMatricula());
		manager.persist(o);
		tx.commit();
		manager.close();


		return true;
	}

	@Override
	public int update(Usuario o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int remove(Usuario o) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public List<Usuario> findAll() {
		
		EntityManager manager = LoadManager.getEntityManager();
		Query query = manager.createQuery("from Usuario");
		List<Usuario> usuarios = query.getResultList();
		return  usuarios;
	}

	@Override
	public Usuario find(Usuario o) {
		EntityManager manager = LoadManager.getEntityManager();
		Query q =  manager.createQuery("FROM  Usuario WHERE  username = 1 AND  password = 2");
		q.setParameter("1", o.getUsername());
		q.setParameter("2", o.getPassword());
		
		return null;
	}

	public int findLogin(Usuario o) {
		EntityManager manager = LoadManager.getEntityManager();
		Query q =  manager.createQuery("FROM  Usuario WHERE  username = 1 AND  password = 2");
		q.setParameter("1", o.getUsername());
		q.setParameter("2", o.getPassword());
		
		return q.executeUpdate();
	}
	
	public ObservableList<Usuario> findAllObservableList()
	{
		ObservableList<Usuario> data = FXCollections.observableArrayList();
		
		EntityManager manager = LoadManager.getEntityManager();
		Query query = manager.createQuery("from usuarios");
		List<Usuario> usuarios = query.getResultList();
		for (Usuario usuario : usuarios) 
		{
			data.add(usuario);
		}
	    return data;	
	}

	
}

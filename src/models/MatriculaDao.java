package models;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import Utils.LoadManager;
import javafx.collections.ObservableList;

public class MatriculaDao extends App_Model implements IPersisteDao<Matricula>{

	@Override
	public boolean save(Matricula o) {
		EntityManager manager = LoadManager.getEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		manager.persist(o);
		tx.commit();
		manager.close();
		return true;
	}

	@Override
	public int update(Matricula o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int remove(Matricula o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Matricula find(Matricula o) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Matricula> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObservableList<Matricula> findAllObservableList() {
		// TODO Auto-generated method stub
		return null;
	}
	



}

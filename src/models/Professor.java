package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;

public class Professor extends App_Model  {

	@Id
	private long id;
	
	@Column(nullable = false)
	private String formacao;
	
	@Column(nullable = false)
	private String nome;
		
	@Column(nullable = false)
	private Date dataNascimento;
	
	
	@Column(nullable = false)		
	private Date data;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFormacao() {
		return formacao;
	}

	public void setFormacao(String formacao) {
		this.formacao = formacao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	
}

package controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import javafx.fxml.FXML;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import Utils.Loader;

public class MainController extends App_Controller {

	
	@FXML
	private  SplitPane splitPane ;
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}

	@FXML
	private void close() {
		System.exit(1);
	}
	
	@FXML
	private void doMinime() {
		Loader.getStage().setIconified(true);
	}
   
	@FXML
	private void inicial() {
		this.routerTela("InicialAdministrador");
	}

	@FXML
	private void usuarios() {
		this.routerTela("UsuariosLista");	
	}

	@FXML
	private void cursos() {
	   this.routerTela("CursosLista");
	}
	@FXML
	private void turmas() {
	   this.routerTela("TurmasLista");
	}
	
	@FXML
	private void disciplinas() {
		this.routerTela("DisciplinasLista");	
	}
	
	@FXML
	private void gruposAcesso() {
		this.routerTela("GrupoAcessoLista");	
	}
	
	
	
	
}

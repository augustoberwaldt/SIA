package controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import components.Session;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.util.Callback;
import models.App_Model;
import models.Curso;
import models.CursoDao;
import models.Disciplina;
import models.DisciplinaDao;
import models.UsuariosDao;

public class CursosController extends App_Controller {

	@FXML
	private TextField codigo;
	@FXML
	private TextField nome;
	@FXML
	private TextField duracao;
	@FXML
	private TextArea descricao;
	@FXML
	private ComboBox comboTurno;
	@FXML
	private ComboBox comboNivel;
	@FXML
	private ListView<Disciplina> listaDisciplinas;

	@FXML
	private TableView tableCurso;

	@FXML
	private TableColumn<Curso, String> codigoColuna;

	@FXML
	private TableColumn<Curso, String> nomeColuna;

	@FXML
	private TableColumn<Curso, String> turnoColuna;

	@FXML
	private TableColumn<Curso, String> nivelColuna;

	@FXML
	private Button editar;

	private final List<Disciplina> disciplina = new ArrayList<Disciplina>();

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		if (comboTurno != null && comboNivel != null) {

			ObservableList<String> optionsTurno = FXCollections
					.observableArrayList("Selecione", "Manh�", "Tarde", "Noite");
			comboTurno.setItems(optionsTurno);
			comboTurno.setValue("Selecione");

			ObservableList<String> optionsNivel = FXCollections
					.observableArrayList("Selecione", "Superior", "Subsequente");
			comboNivel.setItems(optionsNivel);
			comboNivel.setValue("Selecione");

			DisciplinaDao disciplinaDao = new DisciplinaDao();
			List<Disciplina> lista = disciplinaDao.findAll();
			for (int i = 0; i < lista.size(); i++) {
				Disciplina item = lista.get(i);
				item.nomeProperty().addListener((obs, wasOn, isNowOn) -> {});
				listaDisciplinas.getItems().add(item);
			}

			listaDisciplinas.setCellFactory(new CheckBoxListCell()
					.forListView(checkBoxDisciplina()));
			Curso curso = (Curso)  Session.SESSION.get("Curso"); 
			Session.SESSION.clear();
			
			if(curso != null) {
				CursoDao cursoDao = new  CursoDao();             
				curso = cursoDao.find(curso);	
				
			    this.codigo.setText(String.valueOf(curso.getCodigo()));
			    this.nome.setText(curso.getNome());
			    this.duracao.setText(String.valueOf(curso.getNumeroSemestres()));
			    this.descricao.setText(curso.getDescricao());
			    this.comboNivel.setValue(curso.getNivel());
			    this.comboTurno.setValue(curso.getTurno());
			    List<Disciplina> disciplinasck =   this.listaDisciplinas.getItems();
		        this.listaDisciplinas.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

			    for(Disciplina d : curso.getDisciplina() ){
			    	 
			    	  for(Disciplina d2 : disciplinasck ){
			    		  
			    		  if(d2.getId() == d.getId()){
			    		  this.listaDisciplinas.getSelectionModel().select(d2);
			    		
		                 System.out.println("checou");
			    		  }
			    	  }
			    }
			  
			}
		} else {

			CursoDao cursoDao = new CursoDao();  
		    ObservableList<Curso> curso = 	cursoDao.findAllObservableList();
	        if(editar!=null &&  curso.size() > 0 ) {
	        	  editar.setVisible(true); 
			}
			codigoColuna
					.setCellValueFactory(cellData -> new SimpleStringProperty(
							cellData.getValue().codigoProperty().getValue()
									.toString()));
			nomeColuna.setCellValueFactory(cellData -> cellData.getValue()
					.nomeProperty());
			turnoColuna.setCellValueFactory(cellData -> cellData.getValue()
					.turnoProperty());
			nivelColuna.setCellValueFactory(cellData -> cellData.getValue()
					.nivelProperty());
			tableCurso.setItems(curso);
		}

	}

	@FXML 
	private void editar(){
		 
		Curso item	 =(Curso)tableCurso.getSelectionModel().getSelectedItem();	
	    if(item!=null) {
	    	Session.SESSION.put("Curso", item);
			this.routerTela("CursosEditar");	
	    }
		//TODO
	    // messagem de erro
	}
	
	
	@FXML
	private void cadastrar() {
		this.routerTela("CursosCadastro");
	}

	@FXML
	private void salvar() {

		Curso curso = new Curso();

		curso.setCodigo(Integer.parseInt(codigo.getText().toString()));
		curso.setNome(nome.getText());
		curso.setDisciplina(disciplina);
		curso.setDescricao(descricao.getText());

		curso.setTurno(comboTurno.getValue().toString());
		curso.setNivel(comboNivel.getValue().toString());

		CursoDao cursoDao = new CursoDao();

		cursoDao.save(curso);
		
		 Alert alert =  new Alert(javafx.scene.control.Alert.AlertType.CONFIRMATION);
	     alert.setContentText("Regitro Inserido");
		 alert.show();

	}

	@FXML
	private void voltar() {
		this.routerTela("CursosLista");
	}

	private Callback checkBoxDisciplina() {
		return new Callback<Disciplina, ObservableValue<Boolean>>() {
			@Override
			public ObservableValue<Boolean> call(Disciplina item) {

				BooleanProperty observable = new SimpleBooleanProperty();

				observable.addListener((obs, wasSelected, isNowSelected) -> {

				if (isNowSelected) {
					disciplina.add(item);
				} else {
					disciplina.remove(item);
				}
				
				});
				return observable;
			}
		};

	}

}

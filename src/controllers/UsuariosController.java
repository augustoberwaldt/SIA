package controllers;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.ListIterator;
import java.util.Map;
import java.util.Observable;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.text.StyledEditorKit.BoldAction;

import org.hibernate.mapping.List;
import org.json.JSONObject;

import Utils.Loader;
import components.ComponentCorreios;
import javafx.beans.property.IntegerPropertyBase;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import models.Matricula;
import models.MatriculaDao;
import models.Pessoa;
import models.PessoaDao;
import models.Usuario;
import models.UsuarioGrupo;
import models.UsuarioGrupoDao;
import models.UsuariosDao;

public class UsuariosController extends App_Controller {

	@FXML
	private TableView tableUsuarios;

	@FXML
	private TextField matricula;

	@FXML
	private TextField nome;

	@FXML
	private TextField email;

	@FXML
	private TextField cep;

	@FXML
	private TextField endereco;

	@FXML
	private TextField cidade;

	@FXML
	private TextField bairro;

	@FXML
	private TextField uf;

	@FXML
	private TextField complemento;

	@FXML
	private TextField numero;

	@FXML
	private TextField senha;

	@FXML
	private TextField repitasenha;

	@FXML
	private ImageView myImageView;

	@FXML
	private ToggleGroup grupoAcesso;

	@FXML
	private ComboBox<String> statusCombo;

	@FXML
	private TableColumn<Usuario, String> codigo;

	@FXML
	private TableColumn<Usuario, String> nomeUsuario;

	@FXML
	private TableColumn<Usuario, String> matriculaCodigo;

	@FXML
	private TableColumn<Usuario, String> status;

	@FXML
	private TableColumn<Usuario, String> grupoUsuario;

	private RadioButton chk;

	private byte[] foto;

	private Image image;

	ComboBox<String> comboSetor = new ComboBox<String>();
	TextField textSalario = new TextField();
	TextField area = new TextField();
	DatePicker data = new DatePicker();

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		if (grupoAcesso != null) {

			ObservableList<String> optionsStatus = FXCollections.observableArrayList("Ativo", "Inativo");
			statusCombo.setItems(optionsStatus);
			grupoAcesso.selectedToggleProperty().addListener(changeRadioButton());
		} else {

			UsuariosDao usuarioDao = new UsuariosDao();
			codigo.setCellValueFactory(
					cellData -> new SimpleStringProperty(cellData.getValue().idProperty().getValue().toString()));
			matriculaCodigo.setCellValueFactory(cellData -> cellData.getValue().getMatricula().codigoProperty());
			nomeUsuario.setCellValueFactory(cellData -> cellData.getValue().usernameProperty());
			status.setCellValueFactory(ColunaStatus());
			grupoUsuario.setCellValueFactory(cellData -> cellData.getValue().getUsuariogrupos().nomeProperty());

			tableUsuarios.setItems(usuarioDao.findAllObservableList());

		}

	}

	@FXML
	private void add() {

		int idGrupo = 0;
		float salario = 0;
		if (chk.getText().equals("Aluno")) {
			idGrupo = 3;
		} else if (chk.getText().equals("Professor")) {
			salario = idGrupo = 2;
		} else {
			salario = idGrupo = 1;
		}

		Usuario usuario = new Usuario();
		UsuariosDao persiste = new UsuariosDao();
		/* salvando usuario Grupo */
		UsuarioGrupo usuarioGrupo = new UsuarioGrupo();
		usuarioGrupo.setId(idGrupo);
		UsuarioGrupo usugrupo = new UsuarioGrupoDao().find(usuarioGrupo);

		usuario.setStatus(new Integer(1));
		usuario.setPassword(senha.getText());
		usuario.setUsername(nome.getText());
		usuario.setPassword(senha.getText());
		usuario.setUsername(nome.getText());
		usuario.setCep(cep.getText());
		usuario.setEndereco(endereco.getText());
		Calendar currenttime = Calendar.getInstance();
		usuario.setData(new Date((currenttime.getTime()).getTime()));
		usuario.setBairro(bairro.getText());
		usuario.setNumero(Integer.parseInt(numero.getText()));
		usuario.setCidade(cidade.getText());
		usuario.setUf(uf.getText());
		usuario.setComplemento(complemento.getText());
		usuario.setFoto(this.foto);

		/* Salvando Matriz */
		MatriculaDao matriculaDao = new MatriculaDao();
		Matricula matricula = new Matricula();

		matricula.setCodigo(this.matricula.getText());
		matricula.setSituacao(1);
		matricula.setData(new Date((currenttime.getTime()).getTime()));
		matriculaDao.save(matricula);

		/////////////

		PessoaDao pessoaDao = new PessoaDao();
		Pessoa pessoa = new Pessoa();

		usuario.setUsuariogrupos(usugrupo);
		usuario.setMatricula(matricula);

		pessoa.setNome(nome.getText());

		pessoa.setEmail(email.getText());
		pessoa.setStatus((statusCombo.getValue().equals("Ativo") ? 1 : 0));
		pessoa.setTipo(idGrupo);
		usuario.setPessoa(pessoa);
		pessoaDao.save(pessoa);
		persiste.save(usuario);
	}

	@FXML
	private void cadastrar() {
		this.routerTela("UsuariosCadastro");
	}

	@FXML
	private void edit() {

	}

	@FXML
	private void onfocus() {
		cep.focusedProperty().addListener(new ChangeListener<Boolean>() {

			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				bairro.setDisable(false);
				cidade.setDisable(false);
				uf.setDisable(false);
				endereco.setDisable(false);
				System.out.println(newValue);
				/*if (!newValue) {
					ComponentCorreios componet = new ComponentCorreios();

					try {
						JSONObject json = componet.buscaCep(cep.getText().toString());
						boolean err = (boolean) json.get("erro");
                          System.out.println(json.toString()); 
						if (!err) {

							bairro.setText((String) json.get("bairro"));
							cidade.setText((String) json.get("localidade"));
							uf.setText((String) json.get("uf"));
							endereco.setText((String) json.get("logradouro"));
						}

					} catch (IOException e) {
						bairro.setDisable(false);
						cidade.setDisable(false);
						uf.setDisable(false);
						endereco.setDisable(false);

					} catch (Exception e) {
						bairro.setDisable(false);
						cidade.setDisable(false);
						uf.setDisable(false);
						endereco.setDisable(false);
					}
				}*/
			}
		});

	}

	@FXML
	private void abrirArquivo() {
		FileChooser fileChooser = new FileChooser();
		File file = fileChooser.showOpenDialog(Loader.getStage());
		if (file != null) {
			BufferedImage bufferedImage;
			try {
				bufferedImage = ImageIO.read(file);
				System.out.println(file.getPath());
				Path path = FileSystems.getDefault().getPath(file.getPath());

				foto = Files.readAllBytes(path);
				System.out.println(foto);
				image = SwingFXUtils.toFXImage(bufferedImage, null);
				myImageView.setImage(image);
			} catch (IOException e) {

				e.printStackTrace();
			}

		}
	}

	private ChangeListener<Toggle> changeRadioButton() {

		return new ChangeListener<Toggle>() {
			// ComboBox<String> comboSetor = new ComboBox<String>() ;
			// TextField textSalario = new TextField();
			// TextField area = new TextField();
			// DatePicker data = new DatePicker();

			@Override
			public void changed(ObservableValue<? extends Toggle> ov, Toggle t, Toggle t1) {

				Map<String, Parent> map = Loader.getFila();
				AnchorPane pane = (AnchorPane) map.get("UsuariosCadastro");
				chk = (RadioButton) t1.getToggleGroup().getSelectedToggle();

				textSalario.setId("salario");
				textSalario.setPromptText("Salario");
				textSalario.setLayoutX(10.0);
				textSalario.setLayoutY(328.0);
				textSalario.setPrefHeight(35.0);
				textSalario.setPrefWidth(109.0);

				area.setId("AreaAtuacao");
				area.setPromptText("Area de Atua��o");
				area.setLayoutX(10.0);
				area.setLayoutY(328.0);
				area.setPrefHeight(35.0);
				area.setPrefWidth(160.0);

				comboSetor.setId("setor");
				comboSetor.setLayoutX(171.0);
				comboSetor.setLayoutY(328.0);
				comboSetor.setPrefHeight(35.0);
				comboSetor.setPrefWidth(156.0);
				comboSetor.setPromptText("Setor");

				if (chk.getText().equals("Aluno")) {
					pane.getChildren().remove(comboSetor);
					pane.getChildren().remove(comboSetor);
					pane.getChildren().remove(area);
					pane.getChildren().remove(data);
					data.setLayoutX(10.0);
					data.setLayoutY(328.0);
					data.setPrefHeight(35.0);
					data.setPrefWidth(160.0);
					pane.getChildren().add(data);

				} else if (chk.getText().equals("Professor")) {
					pane.getChildren().remove(comboSetor);
					pane.getChildren().remove(textSalario);

					data.setLayoutX(10.0);
					data.setLayoutY(370.0);
					data.setPrefHeight(35.0);
					data.setPrefWidth(160.0);
					pane.getChildren().add(data);
					pane.getChildren().add(area);

				} else {
					pane.getChildren().add(comboSetor);
					pane.getChildren().add(textSalario);
					pane.getChildren().remove(area);
					pane.getChildren().remove(data);
				}

			}
		};

	}

	private Callback<TableColumn.CellDataFeatures<Usuario, String>, ObservableValue<String>> ColunaStatus() {

		return new Callback<TableColumn.CellDataFeatures<Usuario, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(TableColumn.CellDataFeatures<Usuario, String> p) {
				if (p.getValue().getStatus() == 1) {
					return new SimpleStringProperty("Ativo");
				} else {
					return new SimpleStringProperty("Inativo");
				}
			}
		};

	}

	@FXML
	private void voltar() {
		this.routerTela("UsuariosLista");
	}

}
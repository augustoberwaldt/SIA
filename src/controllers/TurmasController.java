package controllers;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.event.ChangeEvent;

import Utils.Loader;
import models.CursoDao;
import models.Disciplina;
import models.DisciplinaDao;
import models.PessoaDao;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.layout.AnchorPane;

public class TurmasController extends App_Controller {
	
	
	@FXML
	private ComboBox<?> comboDisciplinas;
	@FXML
	private ComboBox<String> comboCursos;
	@FXML
	private ComboBox<String> comboNivel;
	@FXML
	private ComboBox<String> comboProfessor;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		DisciplinaDao disciplinaDao = new DisciplinaDao();
		CursoDao   	cursoDao = new 	CursoDao();
		PessoaDao   	pessoaDao = new 	PessoaDao();
		
		
		if (comboNivel != null) {

			ObservableList<String> optionsNivel = FXCollections
					.observableArrayList("Selecione", "Superior", "Integrado");
			comboNivel.setItems(optionsNivel);
			comboNivel.setValue("Selecione");
			ObservableList<String>   listaCurso = cursoDao.findAllObservableListNome();
			listaCurso.add("Selecione");
		    comboCursos.setItems(listaCurso);
		    comboCursos.setValue("Selecione");
		   
		    ObservableList<String>   listaProfessor = pessoaDao.findAllObservableListNomeProfessor();
		    listaProfessor.add("Selecione");
		    comboProfessor.setItems(listaProfessor);
		    comboProfessor.setValue("Selecione");
		   
		    
		}
		
	}
	
	
	@FXML
	private void  cadastrar()
	{
		this.routerTela("TurmaCadastra");
	}
	
	@FXML
	private void voltar() {
		this.routerTela("TurmasLista");
	}

	
	

}

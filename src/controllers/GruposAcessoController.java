package controllers;

import java.net.URL;
import java.util.ResourceBundle;



import javafx.beans.binding.IntegerBinding;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import models.App_Model;
import models.Usuario;
import models.UsuarioGrupo;
import models.UsuarioGrupoDao;
import models.UsuariosDao;

public class GruposAcessoController extends App_Controller {

	
    @FXML
    private  TableView  tablegrupo;
	@FXML
	private TableColumn  <UsuarioGrupo, String> codigo;
	
	@FXML
	private TableColumn  <UsuarioGrupo, String> nome;
	
	@FXML
	private TableColumn  <UsuarioGrupo, String> status;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		UsuarioGrupoDao usuarioGrupoDao  = new  UsuarioGrupoDao();
		codigo.setCellValueFactory( cellData -> new SimpleStringProperty(cellData.getValue().idProperty().getValue().toString() ) );
		nome.setCellValueFactory( cellData -> cellData.getValue().nomeProperty());
		tablegrupo.setItems(usuarioGrupoDao.findAllObservableList());
		
		status.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<UsuarioGrupo, String>, ObservableValue<String>>() {

		    @Override
		    public ObservableValue<String> call(TableColumn.CellDataFeatures<UsuarioGrupo, String> p) {
		        if (p.getValue().getStatus()== 1) {
		        	p.getTableView().setStyle("fx-background-color: yellow");
		        	status.getTableView().setStyle("fx-background-color: yellow");
		            return  new SimpleStringProperty("Ativo") ; 
		        } else {
		        	return  new SimpleStringProperty("Inativo") ;
		        }
		    }
		});
	  
	}	

	
}

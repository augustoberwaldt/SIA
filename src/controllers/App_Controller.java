package controllers;

import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;

import Utils.Loader;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;

public class App_Controller implements Initializable {
	private final static int RIGHT_SIDE = 1; 
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
	
	@FXML
	private void close() {
		System.exit(1);
	}
	
	
	protected void routerTela(String tela) {
		Loader.load(getClass().getResource("/views/"+tela+".fxml"),Loader.getStage() ,false,tela);
		
		Map<String, Parent> map =  Loader.getFila();
		AnchorPane  paneMain    = (AnchorPane) map.get("Main");
		  
		SplitPane splitpane =(SplitPane) paneMain.getChildren().get(3); 
		splitpane.getItems().set(RIGHT_SIDE,(AnchorPane)Loader.getComponet());
	}


	
}

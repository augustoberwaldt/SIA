package controllers;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

import javax.swing.JOptionPane;

import models.Usuario;
import models.UsuariosDao;
import Utils.Encrypt;
import Utils.Loader;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import javafx.stage.Window;

public class LoginController  extends App_Controller{
	
	@FXML
	private TextField usuario;
	@FXML
	private PasswordField senha;
	@FXML
	private Label messagemErro;
	@FXML
	private ToggleGroup grupoAcesso;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		
	}
	
	@FXML
	private void close() {
		System.exit(1);
	}
	
	@FXML
	private void doMinime() {
		Loader.getStage().setIconified(true);
	}
	
	@FXML
	private boolean doLogin() {
	    
		boolean valida = false;

		if(null == usuario  ||  usuario.getText().equalsIgnoreCase("")) {
			valida = true;
		}else if(null == senha  ||  senha.getText().equalsIgnoreCase("")) {
			valida = true;
		}
	
		
	
		Usuario usuarios = new Usuario();
		UsuariosDao usuariosDao = new UsuariosDao();
		
		usuarios.setUsername(usuario.getText());
		usuarios.setPassword(Encrypt.MD5(senha.getText()));
		int result = 0;
	    //result =  usuariosDao.findLogin(usuarios);
		
		if(valida ) {
			this.messagemErro.setVisible(true);
			return false;	  
		}
		
 		Loader.load(getClass().getResource("/views/Main.fxml"), Loader.getStage(),true,"Main");
 		
		return true;
	}
	
	@FXML
	private void removeMessagem() {
		this.messagemErro.setVisible(false);
	}
	
	

	
}

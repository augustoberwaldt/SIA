package controllers;

import java.net.URL;
import java.util.ResourceBundle;

import components.Component;
import components.ComponentDisciplina;
import components.Session;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.util.Callback;
import models.Disciplina;
import models.DisciplinaDao;
import models.UsuariosDao;

public class DisciplinaController  extends App_Controller{

	@FXML
	private TextField id;
	
	@FXML
	private TextField nome;
	
	@FXML
	private TextField ementa;
	
	@FXML
	private TextField objetivo;
	
	@FXML
	private TextArea conteudo;
	
	@FXML
	private TextArea referencia;
	

	@FXML
	private   TableView   tableDisciplina;
	
	@FXML
	private  TableColumn<Disciplina,String> codigoColuna ;
	
	
	@FXML
	private  TableColumn<Disciplina,String> nomeColuna ;
	
	
	@FXML
	private  TableColumn<Disciplina,String> ementaColuna ;
	
	
	@FXML
	private  TableColumn<Disciplina,String> objetivoColuna ;
	
    @FXML
    private Button   btneditar;
    
    @FXML
    private Button   btnexcluir;

	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
       
		DisciplinaDao  disciplinaDao = new DisciplinaDao ();
        if(codigoColuna!=null) {
          ObservableList<Disciplina> disciplina = 	disciplinaDao.findAllObservableList();
          if(btneditar!=null &&  disciplina.size() > 0 ) {
        	  btneditar.setVisible(true); 
        	  btnexcluir.setVisible(true); 
		  }
        	
           codigoColuna.setCellValueFactory( cellData -> new SimpleStringProperty( cellData.getValue().idProperty().getValue().toString())  );
		   nomeColuna.setCellValueFactory( cellData ->cellData.getValue().nomeProperty());
		   ementaColuna.setCellValueFactory( cellData ->cellData.getValue().ementaProperty());
		   objetivoColuna.setCellValueFactory( cellData ->cellData.getValue().objetivoProperty());
		   
		   
		   tableDisciplina.setItems(disciplina);
        }
        else{
       
       	  Disciplina disciplina = (Disciplina) Session.SESSION.get("Disciplina");  

       	  if(disciplina!=null) {
       	    disciplina  = disciplinaDao.find(disciplina);

	      	id.setText(String.valueOf(disciplina.getId())); 
	      	nome.setText(disciplina.getNome());  
	      	ementa.setText(disciplina.getEmenta());
	      	objetivo.setText(disciplina.getObjetivo());
	      	conteudo.setText(disciplina.getConteudo());
	      	referencia.setText(disciplina.getReferencia());
       	  }
        }

	}
	
	
	@FXML 
	private void cadastrar(){
		 
		 this.routerTela("DisciplinasCadastro");
	}
	
	@FXML 
	private void editar(){
		 
		Disciplina item	 =(Disciplina)tableDisciplina.getSelectionModel().getSelectedItem();	
	    if(item!=null) {
	    	Session.SESSION.put("Disciplina", item);
			this.routerTela("DisciplinasEditar");	
	    }
		//TODO
	    // messagem de erro
	}
	
	@FXML 
	private void salvar(){
		 
		 Disciplina disciplina = new Disciplina();
		 
		 disciplina.setNome(this.nome.getText());
		 disciplina.setEmenta(this.ementa.getText());
		 disciplina.setObjetivo(this.objetivo.getText());
		 disciplina.setConteudo(this.conteudo.getText());
		 disciplina.setReferencia(this.referencia.getText());
		
		 DisciplinaDao disciplinaDao = new DisciplinaDao();
		 
		 if(disciplinaDao.save(disciplina))	
		 { 
			 Alert alert =  new Alert(AlertType.CONFIRMATION);
		     alert.setContentText("Regitro Inserido");
			 alert.show();
			 limpaCampos();
		 }
			 
	}
	@FXML 
	private void edit()
	{
		 DisciplinaDao  disciplinaDao = new DisciplinaDao ();
		 Disciplina disciplina =  new  Disciplina();
	   
	   
		 disciplina.setId(Integer.parseInt(this.id.getText()));
	     disciplina.setNome(this.nome.getText());
		 disciplina.setEmenta(this.ementa.getText());
		 disciplina.setObjetivo(this.objetivo.getText());
		 disciplina.setConteudo(this.conteudo.getText());
		 disciplina.setReferencia(this.referencia.getText());
		 
		 disciplinaDao.update(disciplina);
	     
	}
	@FXML
	private void excluir()
	{
		 DisciplinaDao  disciplinaDao = new DisciplinaDao ();
		 Disciplina disciplina =  new  Disciplina();
		 
		 Disciplina item	 = (Disciplina)tableDisciplina.getSelectionModel().getSelectedItem();	
		 disciplina.setId(item.getId());
		 
		 disciplina =  disciplinaDao.find(item);
		 try{
	    	 disciplinaDao.remove(disciplina);
	    	 Alert alert =  new Alert(AlertType.CONFIRMATION);
		     alert.setContentText("Regitro Excluido com Sucesso");
			 alert.show();
			 tableDisciplina.setItems(disciplinaDao.findAllObservableList());
		 }catch(Exception e) {
	     		 
			 Alert alert =  new Alert(AlertType.ERROR);
		     alert.setContentText("Disciplina em Uso");
			 alert.show();	 
		 }
		 
	}
	private void limpaCampos()
	{	
		this.nome.setText("");
		this.ementa.setText("");
		this.objetivo.setText("");
		this.conteudo.setText("");
		this.referencia.setText("");
		
	}

	@FXML
	private void voltar(){
		this.routerTela("DisciplinasLista");
	}
	
	
}
